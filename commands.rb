require 'pg'
require 'telegramAPI'

module Method_Commands
#----------------------------------------------------------------------------#
# CLASS Commands
#----------------------------------------------------------------------------#
    class Commands
        @@dice_markup = {
            "keyboard"=>[["D4", "D6", "D8", "D10"], 
                     ["D12", "D20", "D50", "D100"]],
            "resize_keyboard"=>true,
            "one_time_keyboard"=>true
        }

        def self.start(api, bot, chat_id, usr)
            api.sendMessage(chat_id, "Hi, #{usr['first_name']}! I'm \
#{bot['first_name']}, I can help you to Organize your Role \
Characters and their Inventory, also to Roll the dices, have a \
look to my commands, /help")
        end

        def self.help(api, chat_id)
            api.sendMessage(chat_id,"Bot Commands: \n
/add_character: Adds a new character
 to the bot's database.\n
/delete_character: Deletes a character
 and their items of the bot's database.\n
 /characters: Shows all of your chararcters
 on a list\n
/add_item: Adds a new item to the 
bot's database\n
/roll n: Roll the dices, being 'n' 
the number of dices you would roll, 
then you'll set the dice type.\n
")
        end

        def self.roll_markup(api, chat_id)
            api.sendMessage(chat_id, "Choose the 🎲:", {"reply_markup"=>@@dice_markup})
        end

        def self.roll(api, chat_id, dices, faces)
            rolls = ""
            total = 0
            for i in 0...dices  
                result = rand(1..faces)
                rolls = rolls + "Rolled: #{result}\n"
                total += result
            end
            api.sendMessage(chat_id, "#{rolls}\n\nTotal: #{total}")
        end

        def self.add_character_to_db(api, chat_id, db, username, char_name, gender, age)
            begin
                db.exec("INSERT INTO characters (
                userid,
                name,
                gender,
                age)
                VALUES ( '#{username}', '#{char_name}', '#{gender}', #{age})
                ;").to_a
                api.sendMessage(chat_id, "Your character #{char_name}, has been succesfully created!")
            rescue
                api.sendMessage(chat_id, "That didn't work, try again later\nSorry for the inconvenineces...😔")
                puts "database error creating character"
            end
        end

        def self.character_exists(db, username, char_name)
            exists = db.exec("SELECT userid, name FROM characters
                WHERE userid = '#{username}' AND name = '#{char_name}' LIMIT 1;").to_a
            if exists.length != 1
                return false
            else
                return true
            end
        end

        def self.character_show(api, chat_id, db, username, char_name)
                begin
                    fields = ['userid', 'name', 'gender', 'age']
                    querie = db.exec("SELECT userid, name, gender, age FROM characters
                        WHERE userid = '#{username}' AND name = '#{char_name}';").to_a
                    character = ""
                    fields.each do |key|
                    character += "#{querie[0][key]}\n"
                    end
                    api.sendMessage(chat_id, character)
                rescue Exception => ex
                    puts "Exception ocurred."
                    puts ex
                end
        end

        def self.character_list(api, chat_id, db, username)
            begin
                querie = db.exec("SELECT name FROM characters
                    WHERE userid = '#{username}' ORDER BY name").to_a
                characters = ""
                querie.each do |input|
                    characters += "#{input['name']}\n"
                end
                return characters
            rescue Exception => ex
                puts "Exception ocurred."
                puts ex
            end
        end
        def self.add_item_to_db(api, chat_id, db, username, 
                                char_name, item_name, qntity)
            begin
                db.exec("INSERT INTO items (
                userid,
                c_name,
                name,
                qntity)
                VALUES ( '#{username}', '#{char_name}', '#{item_name}', '#{qntity}')
                ;")
                api.sendMessage(chat_id, "Your item #{item_name}, has been added to #{char_name}")
            rescue Exception => ex
                puts "Exception ocurred."
                puts ex
            end # begin
        end # add_item_to_db
        def self.delete_character(api, chat_id, db, character)
            begin
                db.exec("DELETE FROM characters
                                WHERE name = '#{character}'")
                api.sendMessage(chat_id, "#{character}, has been deleted from the database")
                #api.sendSticker(chat_id, "CAACAgIAAxkBAAIJYV5gsXWEiqqmP6LpZ3WYx4Of7xrSAAKNCgACLw_wBp6pzI0W96HfGAQ")
            rescue Exception => ex
                puts "Exception ocurred."
                puts ex
            end # Begin
        end # Method Delete character
    end # Class Commands
end # Module Method_Commands