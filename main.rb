require 'bundler/setup'
require 'dotenv/load'
require 'telegramAPI'

require_relative 'Apps'

Bundler.require # Takes the modelob commit for the telegramAPI

include Apps

STDOUT.sync = true

#----------------------------------------------------------------------------#
# Constants
#----------------------------------------------------------------------------#
# Check windows
# Check linux
BASELINE_PATH = "database\\baseline.sql"
DB_PATH = "C:\\Users\\Exequam\\AppData\\Roaming\\Party_Setter\\data.db"

#----------------------------------------------------------------------------#
# Required environment variables
#----------------------------------------------------------------------------#
REQUIRED_ENV_VARS = [
    "TELEGRAM_TOKEN", "HEROKU_HOST", "HEROKU_DB", "HEROKU_USER", "PORT", 
    "HEROKU_PSWD"
]

#----------------------------------------------------------------------------#
# Check required environment variables
#----------------------------------------------------------------------------#
REQUIRED_ENV_VARS.each do |env_var|
    if not ENV[env_var]
        puts "missing environment variable: '#{env_var}'" # TODO: to stderr
        exit(false)
    end
end

#----------------------------------------------------------------------------#
# Set variables from environment
#----------------------------------------------------------------------------#
token = ENV['TELEGRAM_TOKEN']
host = ENV['HEROKU_HOST']
dbname = ENV['HEROKU_DB']
db_user = ENV['HEROKU_USER']
pswd = ENV['HEROKU_PSWD']
port = ENV['PORT']
#----------------------------------------------------------------------------#
# Create application
#----------------------------------------------------------------------------#
app = App.new(token, host, port, dbname, db_user, pswd) # DB_PATH, BASELINE_PATH, 

#----------------------------------------------------------------------------#
# Run application
#----------------------------------------------------------------------------#
puts "Starting bot"
begin
    app.run
rescue SystemExit, Interrupt
    puts "Received stop singal"
end

#----------------------------------------------------------------------------#
# Close application
#----------------------------------------------------------------------------#
puts "Closing bot"
app.close

exit(true)
