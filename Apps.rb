require 'pg'
require 'telegramAPI'
require_relative 'commands'
require 'workers'

include Method_Commands
#----------------------------------------------------------------------------#
# Apps Module
#----------------------------------------------------------------------------#
module Apps
#----------------------------------------------------------------------------#
# CLASS App
#----------------------------------------------------------------------------#
    class App
        private
            # Status codes
            @@status_roll = 1
            @@status_name = 2
            @@status_gender = 3
            @@status_age = 4
            @@status_i_char = 5
            @@status_i_name = 6
            @@status_i_qnty = 7
            @@status_del_char = 8
            @@status_del_item = 9

            @@gender_markup = {
                "keyboard"=>[["Male","Female","Other"]],
                "resize_keyboard"=>true,
                "one_time_keyboard"=>true
            }
            # Attributes
            attr_reader :api, :bot, :db, :usr_status, :usr_args

            # Opens API
            def open_api(token)
                # TODO: check errors
                @api = TelegramAPI.new(token)
                @bot = @api.getMe
            end

            # Opens database
            def open_db(host, port, dbname, db_user, pswd)
                # TODO: check errors
                @db = PG::Connection.new( :host => host, 
                        :port => port, :dbname => dbname, 
                        :user => db_user, :password => pswd)
                rescue PG::Error => e
                    puts e.message
            end

            # Sets user status
            def status(usr, status, args)
                @usr_status[usr] = status
                @usr_args[usr] = args
            end

            def clear_status(usr)
                @usr_status.delete(usr)
                @usr_args.delete(usr)
            end

            # Parses an status
            def parse_status(chat_id, usr, message)
                status = @usr_status[usr]
                args = @usr_args[usr]

                case status
                    when @@status_roll
                        faces = message[1..-1].to_i
                        if not ((faces == 4) or (faces == 6) or (faces == 8) or (faces == 10) or
                            (faces == 12) or (faces == 20) or (faces ==50) or (faces == 100))
                            @api.sendMessage(chat_id, "bad number of faces")
                        else
                            Commands.roll(@api, chat_id, args, faces)
                        end
                        clear_status(usr)
                    when @@status_name
                        begin
                            exists = Commands.character_exists(@db, usr, message)
                            if exists
                                @api.sendMessage(chat_id, "🧐... It seems that name for your character alredy exists.\nTry another one...")
                            else
                                status(usr, @@status_gender, {:@@status_name=> message})
                                @api.sendMessage(chat_id, "Which gender have your charcter?", {"reply_markup"=>@@gender_markup})
                            end
                        rescue Exception => ex
                            @api.sendMessage(chat_id, "That didn't work, try again later\nSorry for the inconvenineces...😔")
                            puts "database error checking if character exists: #{ex.message}"
                            clear_status(usr)
                        end
                    when @@status_gender
                        # check gender m/f/o
                        gender = message[0].upcase
                        # if not valid.. ask again same status same args
                        if not (gender == 'M' or gender == 'F' or gender == 'O')
                            @api.sendMessage(chat_id, "Thats not a valid gender for your character.\nTry again please...", {"reply_markup"=>@@gender_markup})
                        else
                            args = @usr_args[usr]
                            args[:@@status_gender] = gender
                            status(usr, @@status_age, args)
                            @api.sendMessage(chat_id, "What age have your character?")
                        end
                    when @@status_age
                        age = message.to_i
                        if age == 0
                            @api.sendMessage(chat_id, "Thats not a valid age for your character.\nTry again please...")
                        else
                            Commands.add_character_to_db(@api, chat_id,
                                @db,
                                usr,
                                @usr_args[usr][:@@status_name],
                                @usr_args[usr][:@@status_gender],
                                age
                            )
                            Commands.character_show(@api, chat_id, @db, usr, @usr_args[usr][:@@status_name])
                            clear_status(usr)
                        end
                    when @@status_i_char
                        exists = Commands.character_exists(@db, usr, message)
                        if not exists
                            @api.sendMessage(chat_id, "🧐... It seems that these character don't exists.\n Try again please...")
                        else
                            status(usr, @@status_i_name, {:@@status_i_char=>message})
                            @api.sendMessage(chat_id, "What name will have the new item?")
                        end
                    when @@status_i_name
                        args = @usr_args[usr]
                        args[:@@status_i_name] = message
                        status(usr, @@status_i_qnty, args)
                        @api.sendMessage(chat_id, "And, How many #{message} will have?")
                    when @@status_i_qnty
                        qnty = message.to_i
                        if qnty == 0
                            @api.sendMessage(chat_id, "Thats not a valid quantity for the item.\nPlease try again...")
                        else
                            Commands.add_item_to_db(@api, chat_id, @db,
                                usr,
                                @usr_args[usr][:@@status_i_char],
                                @usr_args[usr][:@@status_i_name],
                                qnty
                            )
                            clear_status(usr)
                        end
                    when @@status_del_char
                        exists = Commands.character_exists(@db, usr, message)
                        if exists
                            Commands.delete_character(@api, chat_id, @db, message)
                        else
                            @api.sendMessage(chat_id, "#{message} is not a valid character...\nTry again please.")
                        end
                    else
                        @api.sendMessage(chat_id, "unknown status #{status}")
                        clear_status(usr)
                end
            end

            # Parses an update
            def parse(update)
                chat_id = update['message']['chat']['id']
                usr = update['message']['from']['username']
                message = update['message']['text']
                case message
                    when "/start"
                        Commands.start(@api, @bot, chat_id, usr)
                    when "/help"
                        Commands.help(@api, chat_id)
                    when /^\/roll/
                        # Check len
                        if message.length > 5
                            # grab arg
                            arg = message[5..-1].to_i
                            if arg == 0
                                @api.sendMessage(chat_id, "Bad number of dices")
                            else
                                status(usr, @@status_roll, arg)
                                Commands.roll_markup(@api, chat_id)
                            end
                        else
                            arg = 1
                            status(usr, @@status_roll, arg)
                            Commands.roll_markup(@api, chat_id)
                        end
                    when "/add_character"
                        @api.sendMessage(chat_id, "So you wanna create a new character... What name will have?")
                        status(usr, @@status_name, message)
                    when "/characters"
                        @api.sendMessage(chat_id, "Characters of #{usr}:")
                        characters = Commands.character_list(@api, chat_id, @db, usr)
                        @api.sendMessage(chat_id, characters)
                    when "/add_item"
                        characters = Commands.character_list(@api, chat_id, @db, usr)
                        @api.sendMessage(chat_id, "For which character will you add an item?\nPlease type the name correctly\n\n#{characters}")
                        status(usr, @@status_i_char, message)
                    when "/delete_character"
                        characters = Commands.character_list(@api, chat_id, @db, usr)
                        @api.sendMessage(chat_id, "Which character would you like to remove?\n\n#{characters}")
                        status(usr, @@status_del_char, message)
                    when "/delete_item"
                        characters = Commands.character_list(@api, chat_id, @db, usr)
                        @api.sendMessage(chat_id, "From which character?")
                    else
                        if @usr_status.has_key?(usr)
                            parse_status(chat_id, usr, message)
                        else
                            @api.sendMessage(chat_id,"unknown command #{message}")
                        end
                end #case
            end #parse

        public
            # Constructor
            def initialize(token, host, port, dbname, db_user, pswd) #db_path, db_source,
                open_api(token)
                open_db(host, port, dbname, db_user, pswd)
                @usr_status = Hash.new
                @usr_args = Hash.new
                @pool = Workers::Pool.new(:on_exception => proc { |e|
                    puts "A worker encountered an exception: #{e.class}: #{e.message}"
                })
            end

            # Destructor
            def close
                @db.close
                @pool.dispose(10)
            end

            # Run application
            def run
                # TODO: handle stop signal(?)
                while true do
                    updates = @api.getUpdates()
                    updates.each do |update|
                        # Read only the messages of a minute before the Bot starts.
                        if update['message']['date'] >= (Time.now.to_i - 60)
                            @pool.perform do
                                parse(update)
                            end
                        end
                    end
                end
            end
    end
end